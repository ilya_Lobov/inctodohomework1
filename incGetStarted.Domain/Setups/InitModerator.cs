﻿using System.Linq;
using Incoding.Block.IoC;
using Incoding.CQRS;
using System.Collections.Generic;
using System.Collections;

namespace incGetStarted.Domain
{
    public class InitModerator : ISetUp
    {
        public void Dispose()
        {

        }

        public int GetOrder()
        {
            return 2;
        }

        public void Execute()
        {
            var dispatcher = IoCFactory.Instance.TryResolve<IDispatcher>();
         if (dispatcher.Query(new GetModeratorQuery()).Any())
              return;
         var addModerator = new AddModeratorCommand() { Name = "Jon_andersen", Email = "Jon_andersen@le.zu" };
            var addModerator1 = new AddModeratorCommand() { Name = "Bill_smit", Email = "Bill_smit@test.com" };
            dispatcher.Push(addModerator1);
            dispatcher.Push(addModerator);
            string Jon_andersen = addModerator.IdFoIniModer;
            string Bill_smit = addModerator1.IdFoIniModer;
            var addPar = new AddPartCommand() { Comment = "Home_work", Title = "Home", IdModerator = Jon_andersen, Active = false };
            var addPar0 = new AddPartCommand() { Comment = "Car_city", Title = "Car", IdModerator = Jon_andersen, Active = false };
            var addPar1 = new AddPartCommand() { Comment = "kitty_Live", Title = "Kitty", IdModerator = Bill_smit, Active = true };
            dispatcher.Push(addPar1);
            dispatcher.Push(addPar);
            dispatcher.Push(addPar0);
 

        }


    }
}