﻿using Incoding.Block.IoC;
using Incoding.CQRS;
using Incoding.Data;

namespace incGetStarted.Domain
{
    public class InitDataBase : ISetUp
    {
        public void Dispose()
        {

        }

        public int GetOrder()
        {
            return 1;
        }

        public void Execute()
        {
            var managerDb = IoCFactory.Instance.TryResolve<IManagerDataBase>();
            if (!managerDb.IsExist())
                managerDb.Create();
        }
    }
}