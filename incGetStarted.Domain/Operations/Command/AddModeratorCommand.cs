﻿using System.Security.Permissions;
using System.Web.Mvc;
using FluentValidation;
using Incoding.CQRS;
using Remotion.Linq.Utilities;
using System.Collections.Generic;
using System.Collections;
using NHibernate.Dialect.Function;
using System.Linq;

namespace incGetStarted.Domain
{
    //спросить как сделать эфективнее
    public class AddModeratorCommand : CommandBase
    {
        public string Name { get; set; }
        public string Email { get; set; }
      public string IdFoIniModer { get; set; }

        public override void Execute()
        {
            var moderator = new Moderator
                {
                    Name = Name,
                    Email = Email
                    
                };
            IdFoIniModer = moderator.IdFoIniModer;

             Repository.Save(moderator);
        }
    }
}
