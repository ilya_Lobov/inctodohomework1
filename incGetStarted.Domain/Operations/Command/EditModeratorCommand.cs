﻿using Incoding.CQRS;
using Incoding.MvcContrib;

namespace incGetStarted.Domain
{
    public class EditModeratorCommand : CommandBase
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
    

        public override void Execute()
        {
            var moderator = Repository.GetById<Moderator>(Id);
             moderator.Name = Name;
             moderator.Email = Email;
             Repository.SaveOrUpdate(moderator);
        }
    }
}
