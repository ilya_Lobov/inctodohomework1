using Incoding.CQRS;

namespace incGetStarted.Domain
{
    public class ToggleParCommand : CommandBase
    {
        public string Id { get; set; }
    
        public override void Execute()
        {
            var par = Repository.GetById<Par>(Id);            
            par.Active = !par.Active;
        }
    }
}