using Incoding.CQRS;
using Incoding.MvcContrib;

namespace incGetStarted.Domain
{
    public class AddPartCommand : CommandBase
    {
        public bool? Active { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public string IdModerator { get; set; }

        public override void Execute()
        {
            var par = new Par
            {
                Title = Title,
                Comment = Comment,
                Active = Active.GetValueOrDefault(false)
            };
            var thisModer = Repository.GetById<Moderator>(IdModerator);             
            thisModer.AddPart(par);
            Repository.Save(par);
        }

        // public class Validator : AbstractValidator<AddPartCommand>
        // {
        //    public Validator()
        //  {
        //    RuleFor(r => r.Title).NotEmpty().WithMessage("Title should not be empty");                
        //}
        //   }
    }
}