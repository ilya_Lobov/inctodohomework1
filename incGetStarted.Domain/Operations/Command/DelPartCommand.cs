using Incoding.CQRS;

namespace incGetStarted.Domain
{
    public class DelPartCommand : CommandBase
    {
        public string Id_Par { get; set; }

        public override void Execute()
        {
            Repository.Delete<Par>(Id_Par);
        }
    }
}