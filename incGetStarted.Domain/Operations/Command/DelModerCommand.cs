using Incoding.CQRS;

namespace incGetStarted.Domain
{
    public class DelModerCommand : CommandBase
    {
        public string Id_Moder { get; set; }

        public override void Execute()
        {
            Repository.Delete<Moderator>(Id_Moder);
        }
    }
}