﻿using System.Linq;
using Incoding.CQRS;

namespace incGetStarted.Domain
{
    public class GetStatusModeratorQuery : QueryBase<GetStatusModeratorQuery.Response>
    {
        public class Response
        {
            public  int  AllCount { get; set; }
            public  int TrueCount { get; set; }
            public  int FalseCount { get; set; }
        }
        protected override GetStatusModeratorQuery.Response ExecuteResult()
        {
            var customers = Repository.Query<Par>();
            return new GetStatusModeratorQuery.Response
            {
                AllCount = customers.Count(),
                TrueCount = customers.Count(r => r.Active == true),
                FalseCount = customers.Count(r => r.Active == false)
            };
        }
    }
}