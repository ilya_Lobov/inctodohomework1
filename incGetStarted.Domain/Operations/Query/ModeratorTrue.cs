using System;
using System.Linq.Expressions;
using Incoding;

namespace incGetStarted.Domain
{
    public class ModeratorTrue : Specification<Moderator>
    {
        public override Expression<Func<Moderator, bool>> IsSatisfiedBy()
        {
            return m =>true;
        }
    }
}