﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Incoding.CQRS;
using Incoding.Data;
using JetBrains.Annotations;
using NHibernate.Dialect.Function;
using Incoding.Extensions;

namespace incGetStarted.Domain
{
    public class GetModeratorQuery : QueryBase<List<GetModeratorQuery.TempModeratorAndParClass>>
    {
        public class TempModeratorAndParClass
        {
            public string Id_moder2 { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string Id_moder { get; set; }
           // public string Id_par { get; set; }
            public string Comment { get; set; }
            public string Title { get; set; }
            public bool Active { get; set; }

            public IList<TempPar> tempP;
           }
        public class TempPar
      {
            public string id_Par { get; set; }
            public string Comment { get; set; }
           public string Title { get; set; }
           public bool Active { get; set; }
      }
       public class Tmpl
      {
         public string FooterId { get; set; }
         
           public string ContainerId { get; set; }
      }
   
      public string Search { get; set; }
      
        
        protected override List<TempModeratorAndParClass> ExecuteResult()
        {

            return Repository.Query<Moderator>(whereSpecification: new ModeratorTrue())  
                .ToList()
                .Select(r => new TempModeratorAndParClass()
            {
                Id_moder = r.Id.ToString(),
                Name = r.Name,
                Email = r.Email,
                Id_moder2 = r.IdFoIniModer.ToString(),
                tempP = r.parts.Select(par=>new TempPar()
                    {
                        id_Par = par.Id.ToString(),
                        Comment = par.Comment,
                        Title = par.Title,
                        Active = par.Active
                    })
                    .ToList()
            })
            .ToList();
        }
    }
}












