﻿using System;
using FluentNHibernate.Mapping;
using Iesi.Collections.Generic;
using Incoding.Data;
using NHibernate.Criterion;
using StructureMap.Pipeline;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



namespace incGetStarted.Domain
{
    public class Moderator : IncEntityBase
    {

        public virtual string Name { get; set; }
        public virtual string Email { get; set; }
        public virtual int Rang { get; set; }
        public virtual IList<Par> parts { get; set; }
        public virtual string IdFoIniModer { get; set; }
       
        public Moderator()
        {
            Id = Guid.NewGuid().ToString();
            IdFoIniModer = Id.ToString();
            parts = new List<Par>();
        }

        public virtual void AddPart(Par part)
        {
            part.Moder = this;
            parts.Add(part);
        }

        public class Map : ClassMap<Moderator>
        {
            public Map()
            {
                Table("Moderator");
                Id(x => x.Id).GeneratedBy.Assigned().CustomType<string>();
                Map(x => x.Name);
                Map(x => x.Email);
                Map(x => x.Rang);
                HasMany(x => x.parts).KeyColumn("moder").Inverse().Cascade.All();
            }
        }
    }

    public class Par : IncEntityBase
    {
        public virtual bool Active { get; set; }
        public virtual string Title { get; set; }
        public virtual string Comment { get; set; }
        public virtual Moderator Moder { get; set; }
        
        public Par()
        {
            Id = Guid.NewGuid().ToString();
        }
        public class Map : ClassMap<Par>
        {
            public Map()
            {
                Table("Par");
                Id(r => r.Id).GeneratedBy.Assigned().CustomType<string>();
                Map(r => r.Title);
                Map(r => r.Comment);
                Map(r => r.Active);
                References(r => r.Moder,"moder");
            }
        }
    }
}





    



















  