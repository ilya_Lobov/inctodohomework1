﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using incGetStarted.Domain;
using IncGetStarted.UI.Controllers;
using Incoding.Block.Logging;
using Incoding.MvcContrib;

namespace IncGetStarted.UI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas(); 
 Bootstrapper.Start(); 
 new DispatcherController(); // init routes

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ControllerBuilder.Current.SetControllerFactory(new IncControllerFactory());
            Bootstrapper.Start();

        }

        protected void Application_Error()
        {
            var ex = Server.GetLastError();
            LoggingFactory.Instance.LogException(LogType.Fatal,ex);
        }
    }
}