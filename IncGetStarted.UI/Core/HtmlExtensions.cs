﻿using System;
using System.Security.Policy;
using System.Web.Mvc;
using incGetStarted.Domain;
using Incoding.MvcContrib;

namespace IncGetStarted.UI.Core
{
    public static class HtmlExtensions
    {
        public class LoadSetting
        {
            public string Url { get; set; }

            public string Tmpl { get; set; }
        }

        public static MvcHtmlString Load(this HtmlHelper htmlHelper, Action<LoadSetting> action)
        {
            var setting = new LoadSetting();
            action(setting);
            return htmlHelper.When(JqueryBind.None | JqueryBind.InitIncoding)
                .Do()
                .AjaxPost(setting.Url)
                .OnSuccess(dsl =>
                {
                    var urlTmpl = setting.Tmpl;
                    dsl.Self().Core().Insert.WithTemplateByUrl(urlTmpl).Html();
                })
                .AsHtmlAttributes()
                .ToDiv();
        }
    }
}