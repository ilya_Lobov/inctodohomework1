using System;
using FluentNHibernate.Testing.Values;
using incGetStarted.Domain;
using Incoding.CQRS;
using NHibernate.Mapping.ByCode.Impl;

namespace IncGetStarted.UI.Controllers
{
    using System.Web.Mvc;
    using Incoding.MvcContrib;

    public class HomeController : IncControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
               return View("Contact");
        }

        public ActionResult FetchCustomer(GetModeratorQuery query)
        {            
            return IncJson(dispatcher.Query(query));
        }

        public ActionResult ActivStatusCustomer(GetStatusModeratorQuery query)
        {
            return IncJson(dispatcher.Query(query));
        }
        
        public ActionResult AddModerator(AddModeratorCommand command)
        {
            if (!ModelState.IsValid)
                return IncodingResult.Error(ModelState);
            dispatcher.Push(command);
            return IncodingResult.Success();
        }

        public ActionResult AddPat(AddPartCommand command)
        {
            if (!ModelState.IsValid)
                return IncodingResult.Error(ModelState);
            dispatcher.Push(command);
            return IncodingResult.Success();
        }

        public ActionResult DelPat(DelPartCommand command)
        {
            if (!ModelState.IsValid)
                return IncodingResult.Error(ModelState);
            dispatcher.Push(command);
            return IncodingResult.Success();
        }
        public ActionResult DelModer(DelModerCommand command)
        {
            if (!ModelState.IsValid)
                return IncodingResult.Error(ModelState);
            dispatcher.Push(command);
            return IncodingResult.Success();
        }
        public ActionResult EditModer(EditModeratorCommand command)
        {            
            if (!ModelState.IsValid)
                return IncodingResult.Error(ModelState);
            dispatcher.Push(command);
            return IncodingResult.Success();
        }

     }
}